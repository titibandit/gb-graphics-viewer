#include "tile.h"

void get_pixels_from_tile(Tile tile, unsigned char* pixels) {
  for (int l = 0; l < 8; l++) {
    unsigned char lowByte = tile.data[l*2];
    unsigned char highByte = tile.data[l*2+1];

    for (int x = 7, c = 0; x >= 0; x--, c++) {
      unsigned char highBit = (highByte >> x) & (0b00000001);
      unsigned char lowBit = (lowByte >> x) & (0b00000001);
      
      unsigned char color_index = 2*highBit + lowBit;
      pixels[l*8+c] = (3-color_index) * 85;
    }
  }
}
