typedef struct tile Tile;
struct tile {
  char data[16];
};

void get_pixels_from_tile(Tile tile, unsigned char* pixels);
