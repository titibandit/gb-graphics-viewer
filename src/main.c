#include "main.h"

const char *argp_program_version =
  "gb_graphics_viewer " STRINGIZE(gb_graphics_viewer_VERSION_MAJOR) "." STRINGIZE(gb_graphics_viewer_VERSION_MINOR);
const char *argp_program_bug_address =
  "https://codeberg.org/titibandit/gb-graphics-viewer";
static char doc[] =
  "gb_graphics_viewer -- converts a gameboy rom file into a PNG image.";
static char args_doc[] = "input-rom-file output-png-dir";
static struct argp_option options[] = {
  {"split",    's', 0,       0,  "Output one PNG image per ROM bank" },
  {"width",    'w', "WIDTH", 0,  "Width in tiles of the PNG image (default 16)" },
  { 0 }
};
struct arguments {
  char *args[2];      // input-rom-file & output-png-dir
  int split;
  int width;
};

static error_t parse_opt (int key, char *arg, struct argp_state *state) {
  struct arguments *arguments = state->input;

  switch (key) {
  case 's':
    arguments->split = 1;
    break;
  case 'w':
    int width;
    sscanf(arg, "%d", &width);
    arguments->width = width;
    break;
  case ARGP_KEY_ARG:
    if (state->arg_num >= 2)
      argp_usage (state);
    arguments->args[state->arg_num] = arg;
    break;
  case ARGP_KEY_END:
    if (state->arg_num < 2)
      argp_usage (state);
    break;
  default:
    return ARGP_ERR_UNKNOWN;
  }
  return 0;
}
static struct argp argp = { options, parse_opt, args_doc, doc };

int main(int argc, char **argv) {
  struct arguments arguments;
  // default values
  arguments.split = 0;
  arguments.width = 16;
  
  argp_parse(&argp, argc, argv, 0, 0, &arguments);
  // params
  char *filename = arguments.args[0];
  char *output_dir = arguments.args[1];
  int split = arguments.split;
  int tiles_per_row = arguments.width;

  FILE *rom = fopen(filename, "rb");
  if (rom == NULL) {
    printf("Failed to open file %s.\n", filename);
    return 1;
  }
  // make output directory 
  if (mkdir(output_dir, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) == -1 && errno != EEXIST) {
    printf("Failed to use or create the output directory %s.\n", output_dir);
    return 1;
  }
  
  // compute output path
  char output_path[FILENAME_MAX];
  const char *file_basename;
  size_t l;

  cwk_path_get_basename(filename, &file_basename, &l);
  cwk_path_join(output_dir, file_basename, output_path, sizeof(output_path));

  // truncate extension if any
  const char *extension;
  if(cwk_path_get_extension(output_path, &extension, &l)) {
    *(output_path + (extension - output_path)) = '\0';
  }

  return convert(rom, tiles_per_row, split, output_path);
}
