#define _GNU_SOURCE

#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <png.h>

#include "tile.h"

struct png_image {
  png_controlp opaque;  // Initialize to NULL, free with png_image_free
  png_uint_32  version; // Set to PNG_IMAGE_VERSION
  png_uint_32  width;   // Image width in pixels (columns)
  png_uint_32  height;  // Image height in pixels (rows)
  png_uint_32  format;  // Image format as defined below
  png_uint_32  flags;   // A bit mask containing informational flags
  png_uint_32  colormap_entries; // Number of entries in the color-map
  png_uint_32  warning_or_error;
  char         message[64];
};

int convert(FILE* rom, int tiles_per_row, int split, char* output_path);
static int write_tiles_to_image(Tile* tiles, int tile_count, int tiles_per_row, char* filename);
