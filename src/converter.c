#include "converter.h"

int convert(FILE* rom, int tiles_per_row, int split, char* output_path) {
  // get file length
  fseek(rom, 0, SEEK_END);
  int rom_byte_count = ftell(rom);
  fseek(rom, 0, SEEK_SET);
  // get bytes
  unsigned char bytes[rom_byte_count];
  fread(bytes, 1, rom_byte_count, rom);
  fclose(rom);
  
  int bank_count = 1;
  int tile_count = rom_byte_count / 16;
  int bytes_per_bank = rom_byte_count;

  if (split) {
    bank_count = rom_byte_count / (16 * 1024);
    tile_count = 1024;
    bytes_per_bank = 16*1024;
  }
  
  for (int i = 0; i < bank_count; i++) {
    // select the bytes
    unsigned char current_bytes[bytes_per_bank];
    for (int j = 0; j < bytes_per_bank; j++) {
      current_bytes[j] = bytes[i * (16*1024) + j];
    }

    Tile tiles[tile_count];
  
    for (int i = 0; i < tile_count; i++) {
      struct tile t;
      for (int j = 0; j < 16; j++) {
        t.data[j] = current_bytes[ i*16 + j ];
      }
      tiles[i] = t;
    }
    char *output_filename;
    asprintf(&output_filename, "%s%d.png", output_path, i);
    write_tiles_to_image(tiles, tile_count, tiles_per_row, output_filename);
  }
  return 0;
}

int write_tiles_to_image(Tile* tiles, int tile_count, int tiles_per_row, char* filename) {

  int png_tilerow_count = (int) ceil((float)tile_count / tiles_per_row);
  unsigned char data[png_tilerow_count * 8 * 8 * tiles_per_row];
  void *buffer = &data;
  unsigned char pixels[64];

  png_image png_img = {};

  png_img.format = PNG_FORMAT_GRAY;
  png_img.version = PNG_IMAGE_VERSION;
  png_img.opaque = NULL;
  png_img.width = tiles_per_row * 8;
  png_img.height = png_tilerow_count * 8;

  for (int i = 0; i < tile_count; i++) {
    int T_x = i/tiles_per_row;
    int T_y = (int) fmod(i, tiles_per_row);
    int x_offset = T_y * 8;
    int y_offset = T_x * 8;
    
    get_pixels_from_tile(tiles[i], pixels);
    
    for (int x = 0; x < 8; x++) {
      for (int y = 0; y < 8; y++) {
        int x_png = x_offset + x;
        int y_png = y_offset + y;
        data[y_png*(8*tiles_per_row)+x_png] = pixels[y*8+x];
      }
    }
  }

  
  png_image_write_to_file(&png_img, filename,
                          false, // int convert_to_8bit
                          buffer,
                          tiles_per_row * 8, // png_int_32 row_stride
                          NULL // const void *colormap
                          );
  return 0;  
}
