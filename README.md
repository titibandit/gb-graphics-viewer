# gb_graphics_viewer
Small utility program that converts a gameboy rom into one or several PNG images representing its tile data (and its code for that matter).
Inspired by `https://github.com/taylus/gb-rom-viewer`.

## Build
Clone the repository (with its submodules) and navigate to its repository in a terminal.
Then, build the program using CMake:

```sh
  mkdir build && cd build
  cmake ..
  cmake --build .
```

## Use
Learn how to use the program by using the `help` switch:
```sh
  ./gb_graphics_viewer --help
  
  Usage: gb_graphics_viewer [OPTION...] input-rom-file output-png-dir
  gb_graphics_viewer -- converts a gameboy rom file into a PNG image.
  
    -s, --split                Output one PNG image per ROM bank
    -w, --width=WIDTH          Width in tiles of the PNG image (default 16)
    -?, --help                 Give this help list
    --usage                Give a short usage message
    -V, --version              Print program version
```
